const rows = 20;
const cols = 20;
let cellWidth;
let cellHeight;
let canvas;
let ctx;
let Glim = {direction: 0, positions: [], length: 1};
let pickups = [];
let gameOver = false;
let time = 0;
let headImg = new Image();
let straightImg = new Image();
let tailImg = new Image();
let cornerImg = new Image();
let pickupImg = new Image();

let width;
let height;
window.onload = () => {
    canvas = document.getElementById('board');
    ctx = canvas.getContext('2d');
    ctx.imageSmoothingEnabled = false;
    setSize();
    window.highScore = 0;
    headImg.src = './head.png';
    tailImg.src = './tail.png';
    straightImg.src = './straight.png';
    cornerImg.src = './corner.png';
    pickupImg.src = './book.png';
    init();
}

function setSize(){
    const size = Math.min(window.innerWidth, window.innerHeight) - 48;
    canvas.setAttribute('width', size);
    canvas.setAttribute('height', size);
    cellWidth = Math.floor(canvas.getAttribute('width') / cols);
    cellHeight = Math.floor(canvas.getAttribute('height') / rows);
}

function init(){
    Glim = {
        positions: [[Math.floor(cols  / 2), Math.floor(rows / 2)]],
        direction: 0,
        length: 1,
        nextDirection: 0
    }
    time = 0;
    pickups = [];
    gameOver = false;
    drawFrame();
}

function updateGlim() {
    if(!(
        (Glim.direction === 0 && Glim.nextDirection === 2) ||
        (Glim.direction === 1 && Glim.nextDirection === 3) ||
        (Glim.direction === 2 && Glim.nextDirection === 0) ||
        (Glim.direction === 3 && Glim.nextDirection === 1)
    )){
        Glim.direction = Glim.nextDirection;
    }
    let [headX, headY] = Glim.positions[0];
    let newHead;

    let grid = [];
    for(let x = 0; x < cols; x++){
        let column = [];
        for(let y = 0; y < rows; y++){
            column.push(false);
        }
        grid.push(column);
    }

    for(let i = 0; i < Glim.positions.length - 1; i++){
        let [x, y] = Glim.positions[i];
        grid[x][y] = true;
    }
    switch(Glim.direction){
        case 0:
            newHead = [headX + 1, headY];
            break;
        case 1: 
            newHead = [headX, headY + 1];
            break;
        case 2:
            newHead = [headX - 1, headY];
            break;
        case 3: 
            newHead = [headX, headY - 1];
            break;
    }

    if(newHead[0] < 0 || newHead[0] >= cols || newHead[1] < 0 || newHead[1] >= rows || grid[newHead[0]][newHead[1]]){
        gameOver = true;
        return;
    }

    for(let p of pickups){
        let [px, py] = p.pos;
        let [gx, gy] = newHead;
        if(gx == px && gy == py){
            Glim.length += 1;
            p.ttl = 0;
            Glim.pickedUp = true;
            break;
        }
    }

    Glim.positions.unshift(newHead);
    if(Glim.length < Glim.positions.length){
        Glim.positions.pop();
    }
    return
}

function drawGlim() {
    ctx.fillStyle = '#aa88aa';
    //Draw head
    ctx.save();
    let [x, y] = Glim.positions[0];
    ctx.translate((x * cellWidth + cellWidth / 2), (y * cellHeight + cellHeight / 2));
    if(Glim.direction == 1){
        ctx.rotate(Math.PI / 2);
    }
    if(Glim.direction == 2){
        ctx.scale(-1, 1);
    }
    if(Glim.direction == 3){
        ctx.rotate(-Math.PI / 2);
    }
    ctx.translate(-(x * cellWidth + cellWidth / 2), -(y * cellHeight + cellHeight / 2));
    ctx.drawImage(headImg, x * cellWidth, y * cellHeight, cellWidth, cellHeight);
    ctx.restore();
    for(let i = 1; i < Glim.positions.length - 1; i++){
        let [cx, cy] = Glim.positions[i];//Current position
        let [px, py] = Glim.positions[i - 1];//Previous position

        //Check for corner
        if(i >=1 && i < Glim.positions.length - 1){
            let [nx, ny] = Glim.positions[i + 1];//Next position
            let deltax = nx - px;
            let deltay = ny - py;
            //Both coordinates are different: we've turned
            if(deltax !== 0 && deltay !== 0){
                ctx.save();
                ctx.translate((cx * cellWidth + cellWidth / 2), (cy * cellHeight + cellHeight / 2));
                //Check if we're clockwise or counter-clockwise at each turn
                if(deltax === 1 && deltay === -1){
                    if(cy === ny){
                    ctx.rotate(0);
                    }else{
                        ctx.rotate(Math.PI / 2 * 2);
                    }
                } else if(deltax === 1 && deltay === 1){
                    if(cx === nx){
                    ctx.rotate(Math.PI / 2);
                    }else{
                        ctx.rotate(Math.PI / 2 * 3);
                    }
                } else if(deltax === -1 && deltay === 1){
                    if(cy === ny){
                    ctx.rotate(Math.PI / 2 * 2);
                    }else {
                        ctx.rotate(Math.PI / 2 * 0);
                    }
                } else if(deltax === -1 && deltay === -1){
                    if(cx === nx){
                    ctx.rotate(Math.PI / 2 * 3);
                    }else{
                        ctx.rotate(Math.PI / 2)
                    }
                }
                ctx.translate(-(cx * cellWidth + cellWidth / 2), -(cy * cellHeight + cellHeight / 2));
                ctx.drawImage(cornerImg, cx * cellWidth, cy * cellHeight, cellWidth, cellHeight);
                ctx.restore();
                //Don't draw the straight if we did the corner
                continue;
            }
        }



        const dx = cx - px;
        const dy  = cy - py;
        ctx.save();
        ctx.translate((cx * cellWidth + cellWidth / 2), (cy * cellHeight + cellHeight / 2));
        //Vertical
        if(dy == 1 || dy == -1){
            ctx.rotate(Math.PI / 2);
        }
        //Horizontal
        if(dx == 1 || dx == -1){
            ctx.rotate(0);
        }
        ctx.translate(-(cx * cellWidth + cellWidth / 2), -(cy * cellHeight + cellHeight / 2));
        ctx.drawImage(straightImg, cx * cellWidth, cy * cellHeight, cellWidth, cellHeight);
        ctx.restore();
    }
    if(Glim.length > 1){
        let [tx, ty] = Glim.positions[Glim.length - 1];
        let [px, py] = Glim.positions[Glim.length - 2];
        let deltax = tx - px;
        let deltay = ty - py;
        ctx.save();
        ctx.translate((tx * cellWidth + cellWidth / 2), (ty * cellHeight + cellHeight / 2));
        if(deltay === -1){
            ctx.rotate(Math.PI / 2);
        }

        if(deltay === 1){
            ctx.rotate(-Math.PI / 2);
        }
        if(deltax == 1){
            ctx.scale(-1, 1);
        }
        ctx.translate(-(tx * cellWidth + cellWidth / 2), -(ty * cellHeight + cellHeight / 2));
        ctx.drawImage(tailImg, tx * cellWidth, ty * cellHeight, cellWidth, cellHeight);
        ctx.restore();

        if(Glim.pickedUp){
            Glim.pickedUp = false;
            let head = Glim.positions[0];
            ctx.fillStyle = 'rgba(150, 150, 250, .8)'
            ctx.beginPath();
            ctx.arc(head[0] * cellWidth + cellWidth / 2, head[1] * cellHeight + cellHeight / 2, cellWidth / 1.5, 0, Math.PI * 2);
            ctx.fill();
        }
    }

    let score = document.getElementById('score');
    score.innerHTML = `Score: ${Glim.length}`;
}

function spawnPickup(){
    let x = Math.floor(Math.random() * cols);
    let y = Math.floor(Math.random() * rows);
    pickups.push({pos: [x, y], ttl: 30 + Math.floor(Math.random() * 5)});
}

function drawFrame(){
    setSize();
    drawBackground();
    drawPickups();
    updateGlim();
    drawGlim();



    if(!gameOver){

        setTimeout(drawFrame, 175);
    }else{

        doGameOver();
    }
    time += 1;
}

window.onkeydown = (ev) =>{
    console.log(ev.key);
    if(ev.key == 'ArrowRight' || ev.key == 'd'){
        Glim.nextDirection = 0;
    }else if(ev.key == 'ArrowDown' || ev.key == 's'){
        Glim.nextDirection = 1;
    }else if(ev.key == 'ArrowLeft' || ev.key == 'a'){
        Glim.nextDirection = 2;
    }else if(ev.key == 'ArrowUp' || ev.key == 'w'){
        Glim.nextDirection = 3;
    } 

    if(ev.key === ' ' && gameOver){
        init();
    }
}


function drawBackground(){
    ctx.fillStyle = '#fff';
    ctx.fillRect(0, 0, canvas.getAttribute('width'), canvas.getAttribute('height'))
    ctx.fillStyle = '#222f22';
    for(let x = 0; x < cols; x++){
        for(let y = 0; y < rows; y++){
            ctx.fillRect(x * cellWidth, y * cellHeight, cellWidth - 1, cellHeight - 1);
        }
    }
}

function drawPickups(){
    if(time % 20 == 0){
        spawnPickup();
    }
    ctx.fillStyle = '#f00';
    let toRemove = [];
    for(let i = 0; i < pickups.length; i++){
        let p = pickups[i];
        if(p.ttl == 0){
            toRemove.push(i);
            continue;
        }

        ctx.drawImage(pickupImg, p.pos[0] * cellWidth, p.pos[1] * cellHeight, cellWidth - 1, cellHeight - 1);
        p.ttl--;
    }
    for(let idx of toRemove){
        pickups.splice(idx, 1);
    }
}

function doGameOver(){
    ctx.fillStyle = '#fff';
    ctx.textAlign = 'center'
    ctx.font = '60px sans-serif';
    ctx.fillText("Game Over", canvas.getAttribute('width') / 2, canvas.getAttribute('height') / 3);
    ctx.font = '40px sans-serif';
    ctx.fillText(`Score: ${Glim.length}`, canvas.getAttribute('width') / 2, canvas.getAttribute('height') / 2.5);
    if(Glim.length > window.highScore){
        window.highScore = Glim.length;
    }
    ctx.fillText(`High Score: ${window.highScore}`, canvas.getAttribute('width') / 2, canvas.getAttribute('height') / 2);
    ctx.fillText(`Press Space to play again`, canvas.getAttribute('width') / 2, canvas.getAttribute('height') / 1.5);

}